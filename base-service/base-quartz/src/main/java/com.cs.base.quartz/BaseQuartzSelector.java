package com.cs.base.quartz;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangjiahao
 * @version 1.0
 * @className BaseQuartzSelector
 * @since 2019-03-18 15:18
 */
@Configuration
@ComponentScan("com.cs.base.quartz")
public class BaseQuartzSelector {
}
