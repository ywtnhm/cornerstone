/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: Simple
 * Author:   liyuan
 * Date:     2019-03-19 11:51
 * Description: simple
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.bs.es.entity;


import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * 〈一句话功能简述〉<br>
 * 〈simple〉
 *
 * @author liyuan
 * @create 2019-03-19
 * @since 1.0.0
 */
@Data
public class Simple {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private String content;

    private BigDecimal summary;


}