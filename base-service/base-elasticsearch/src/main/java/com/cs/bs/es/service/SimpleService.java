///**
// * Copyright (C), 2015-2019, XXX有限公司
// * FileName: SimpleService
// * Author:   liyuan
// * Date:     2019-03-21 15:37
// * Description:
// * History:
// * <author>          <time>          <version>          <desc>
// * 作者姓名           修改时间           版本号              描述
// */
//package com.cs.bs.es.service;
//
//import com.cs.bs.es.dao.SimpleRepository;
//import com.cs.bs.es.entity.Simple;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * 〈一句话功能简述〉<br>
// * 〈〉
// *
// * @author liyuan
// * @create 2019-03-21
// * @since 1.0.0
// */
//@Slf4j
//@Service
//public class SimpleService {
//
//    @Autowired
//    private SimpleRepository repository;
//
//
//    public List<Simple> findAll() {
//        return repository.findAll();
//    }
//
//    @Transactional(rollbackFor = Exception.class)
//    public Simple saveOne(Simple entity) {
//        return repository.save(entity);
//    }
//
//}