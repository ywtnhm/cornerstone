package com.cs.micro.demo.quartz2.web;

import com.cs.base.quartz.core.QuartzManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjiahao
 * @version 1.0
 * @className QuartzCtrl
 * @since 2019-03-19 16:33
 */
@RestController
@RequestMapping("/quartz")
public class QuartzCtrl {

    @Autowired
    private QuartzManager manager;

    @GetMapping("/pause")
    public String pause() {
        manager.pauseTrigger("demoTaskTrigger2");
        return "ok";
    }

}
